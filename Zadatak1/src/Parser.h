#ifndef PARSER_H_
#define PARSER_H_

#include <vector>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
class Symbol;
class Relocation;
class Table;
class Assembler;

class Parser {
public:
	Parser();
	~Parser();

	string section;
	int indexOfContainer;
	int serialNumForSections;
	int serialNumForOthers;
	vector<int> locationCounters;
	int flags;

	vector<string> parseInputFile(const string &text);
	void parseLineTokens(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass, int indexOfLine);

	bool doSection(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass);
	bool doLabel(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass);
	bool doOperation(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass, int indexOfLine);
	bool doDirective(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass);
	void doAddressingMode(string token, int& locationCounter, int numOfLine, bool firstPass, bool Wflag, bool dst, int indexOfLine);

	void doImmedValue(string token, int& locationCounter, bool firstPass, bool Wflag);
	bool doImmedSymbol(string name, int& locationCounter, bool firstPass, bool Wflag);
	void doRegDir(int& locationCounter, bool firstPass, bool Wflag, int numOfReg, string high);
	void doRegIndValue(int& locationCounter, bool firstPass, int numOfReg, string num);
	bool doRegIndSymbol(int& locationCounter, bool firstPass, int numOfReg, string name, int indexOfLine);
	bool doAbsSymbol(int& locationCounter, bool firstPass, string name);
	void doAbsValue(int& locationCounter, bool firstPass, string num);

	int returnNumOfOperands(string name);
	bool checkIfSymbolAlreadyExists(string label);
	bool checkIfHelperSymbolAlreadyExists(string label);
	int checkIfEQUSimbolAlreadyExists(string label);
	bool isCorrectNameForSymbol(string name);
	int getNextOperationLocation(int indexOfLine);
	Symbol* getSectionSymbolForSymbol(Symbol* symb);
	bool isNumber(string name);
	bool isCorrectName(string name);
	string intToHex(int i);
	string intToStr(int i);
	int strToInt(string s);
};

#endif
