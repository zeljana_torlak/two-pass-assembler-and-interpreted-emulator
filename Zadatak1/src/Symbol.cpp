#include "Symbol.h"

Symbol::Symbol(string _label, string _section, int _offset,
		string _locality, int _serialNum) {
	label = _label;
	section = _section;
	offset = _offset;
	locality = _locality;
	serialNum = _serialNum;

	size = 0;
	flags = 0;
}

Symbol::~Symbol() {
}

