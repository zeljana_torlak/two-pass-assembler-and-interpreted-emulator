#include "Assembler.h"
#include "Symbol.h"
#include "Relocation.h"

Table Assembler::tableOfSymbols;
Table Assembler::relocationRecords;
vector<vector<string> > Assembler::container;
vector<string> Assembler::EQUsimbols;

Table Assembler::tableOfSymbolsWithoutSections;

Assembler::Assembler() {
}

Assembler::~Assembler() {
	reset();
}

void Assembler::reset(){
	locationCounter = 0;
	tokens.clear();
	numOfLine.clear();
	tableOfSymbols.clear();
	relocationRecords.clear();
	container.clear();
}

void Assembler::initialize(ifstream& inputFile) {
	int numOfLineForTokens = 0;
	string line;
	while (getline(inputFile, line)) {
		numOfLineForTokens++;
		vector<string> lineTokens = myParser.parseInputFile(line);
		if (lineTokens.size() == 0)
			continue;
		while (lineTokens[0][lineTokens[0].length() - 1] == ':') {
			vector<string> temp;
			temp.push_back(lineTokens[0]);
			tokens.push_back(temp);
			numOfLine.push_back(numOfLineForTokens);
			lineTokens.erase(lineTokens.begin());
			if (lineTokens.size() == 0)
				break;
		}
		if (lineTokens.size() == 0)
			continue;
		if (lineTokens[0] == ".end") {
			vector<string> temp;
			temp.push_back(lineTokens[0]);
			tokens.push_back(temp);
			numOfLine.push_back(numOfLineForTokens);
			break;
		}
		tokens.push_back(lineTokens);
		numOfLine.push_back(numOfLineForTokens);
	}

	Symbol* UNDSection = new Symbol("?", "?", 0, "local", 0);
	UNDSection->flags = 15;
	tableOfSymbols.add(UNDSection, UNDSection->serialNum);
}

void Assembler::firstPass() {
	locationCounter = 0;
	myParser.flags = 15;

	for (unsigned i = 0; i < tokens.size(); i++)
		myParser.parseLineTokens(tokens[i], locationCounter, numOfLine[i], true, i);

	myParser.locationCounters.push_back(locationCounter);

	int num = tableOfSymbols.size;
	Assembler::tableOfSymbols.findBySerialNumber(num-1);
	((Symbol*) Assembler::tableOfSymbols.getCurrentData())->size = locationCounter;

	for (tableOfSymbolsWithoutSections.onFirst(); tableOfSymbolsWithoutSections.isCurrent(); tableOfSymbolsWithoutSections.onNext()){
		Symbol* symb = (Symbol*) tableOfSymbolsWithoutSections.getCurrentData();
		symb->serialNum += num;
		tableOfSymbols.add(symb, symb->serialNum);
	}
	tableOfSymbolsWithoutSections.clear();
}

void Assembler::secondPass() {
	locationCounter = 0;
	myParser.flags = 15;
	vector<string> helper;
	container.push_back(helper);
	myParser.section = "?";
	for (unsigned i = 0; i < tokens.size(); i++)
		myParser.parseLineTokens(tokens[i], locationCounter, numOfLine[i], false, i);
}

void Assembler::generateOutput(ofstream& outputFile) {
	outputFile << "#begin_Table_of_Symbols:" << endl;
	outputFile << "label\t section\t offset\t locality\t serialNum" << endl;
	for (tableOfSymbols.onFirst(); tableOfSymbols.isCurrent(); tableOfSymbols.onNext()) {
		Symbol* temp = (Symbol*) tableOfSymbols.getCurrentData();
		string label = temp->label;
		string section = temp->section;
		while (label.length()<3)
			label.push_back(' ');
		while (section.length()<3)
			section.push_back(' ');
		outputFile << label << "     \t" << section << "     \t"
				<< temp->offset << "     \t" << temp->locality << "     \t"
				<< temp->serialNum << endl;
	}
	outputFile << "#end_Table_of_Symbols" << endl;

	outputFile<< endl;
	tableOfSymbols.onFirst();
	for (unsigned i = 0; i < container.size(); i++){
		if (container[i].size() != 0) {
			Symbol* curr = (Symbol*) tableOfSymbols.getCurrentData();

			outputFile << "#section_" << curr->label << endl;
			if ((curr->flags & 0x01) != 0){
				outputFile << "#begin_Relocation_Records:" << endl;
				outputFile << "offset\t type\t serialNum" << endl;
				for (relocationRecords.onFirst(); relocationRecords.isCurrent(); relocationRecords.onNext()) {
					Relocation* temp = (Relocation*) relocationRecords.getCurrentData();
					if (temp->section == curr->section){
						string section = temp->section;
						while (section.length()<3)
							section.push_back(' ');
						outputFile << temp->offset << "     \t" << temp->type
								<< "     \t" << temp->serialNum << endl;
					}
				}
				outputFile << "#end__Relocation_Records" << endl;


				for (unsigned j = 0; j < container[i].size(); j++){
					if (j%10 == 0) outputFile << endl;
					outputFile << container[i][j] << " ";
				}
			} else {
				outputFile << "#section_size_" << curr->size << endl;
			}
			outputFile << endl << endl;
		}
		tableOfSymbols.onNext();
	}
}
