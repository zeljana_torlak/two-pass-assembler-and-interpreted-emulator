#ifndef SYMBOL_H_
#define SYMBOL_H_

#include <iostream>
#include <string>
using namespace std;

class Symbol {
public:
	string label;
	string section;
	int offset;
	string locality;
	int serialNum;

	int flags;
	int size;

	Symbol(string _label, string _section, int _offset, string _locality, int _serialNum);
	~Symbol();
};

#endif
