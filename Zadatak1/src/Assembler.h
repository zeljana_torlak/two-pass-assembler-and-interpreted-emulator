#ifndef ASSEMBLER_H_
#define ASSEMBLER_H_

#include "Table.h"
#include "Parser.h"
using namespace std;
class Symbol;
class Relocation;

class Assembler {
public:
	Assembler();
	~Assembler();

	int locationCounter;
	Parser myParser;
	vector<vector<string> > tokens;
	vector<int> numOfLine;

	void initialize(ifstream& output);
	void firstPass();
	void secondPass();
	void generateOutput(ofstream& output);
	void reset();

	static Table tableOfSymbols;
	static Table relocationRecords;
	static vector<vector<string> > container;
	static vector<string> EQUsimbols;

	static Table tableOfSymbolsWithoutSections;
};

#endif
