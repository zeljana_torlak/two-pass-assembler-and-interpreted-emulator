#include "Assembler.h"
#include <stdlib.h> //exit
using namespace std;

int main(int argc, char* argv[]) {

	ifstream inputFile;
	ofstream outputFile;
	inputFile.open(argv[4]);
	outputFile.open(argv[3]);

	if (!inputFile.is_open()) {
		cerr << "Error while opening file: " << argv[4] << endl;
		exit(1);
	} else if (!outputFile.is_open()) {
		cerr << "Error while opening file: " << argv[3] << endl;
		exit(1);
	}

	Assembler assembler;
	try {
		assembler.initialize(inputFile);
		assembler.firstPass();
		assembler.secondPass();
		assembler.generateOutput(outputFile);
	} catch (const exception& e) {
		cerr << e.what() << "\nFatal error" << endl;
	}

	inputFile.close();
	outputFile.close();
	cout << "\nPolar bear" << endl;
	return 0;
}
