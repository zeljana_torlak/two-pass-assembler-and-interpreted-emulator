#include "Parser.h"
#include "Table.h"
#include "Symbol.h"
#include "Relocation.h"
#include "Assembler.h"
#include <stdexcept>
#include <sstream>
#include <iomanip>

Parser::Parser() {
	section = "?"; indexOfContainer = 0;
	serialNumForSections = 1;
	serialNumForOthers = 0;
}

Parser::~Parser() {
}

vector<string> Parser::parseInputFile(const string &text) {
	string delims = " ,\t"; //not totally correct solution
	vector<string> tokens;
	size_t start = text.find_first_not_of(delims), end = 0;

	while ((end = text.find_first_of(delims, start)) != string::npos) {
		tokens.push_back(text.substr(start, end - start));
		start = text.find_first_not_of(delims, end);
	}
	if (start != string::npos)
		tokens.push_back(text.substr(start));

	return tokens;
}

void Parser::parseLineTokens(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass, int indexOfLine) {
	if (firstPass) locationCounters.push_back(locationCounter);
	if (!doSection(lineTokens, locationCounter, numOfLine, firstPass) && !doLabel(lineTokens, locationCounter, numOfLine, firstPass) &&
			!doDirective(lineTokens, locationCounter, numOfLine,firstPass) && !doOperation(lineTokens, locationCounter, numOfLine, firstPass, indexOfLine))
		throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Could not be resolved.");
}

bool Parser::doSection(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass) {
	string label;
	if (lineTokens[0] == ".section"){
		if (lineTokens.size() < 2 || lineTokens[1].length() < 2 || lineTokens[1][0] != '.')
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid section name.");

		section = lineTokens[1].substr(1, lineTokens[1].length() - 1);
		label = lineTokens[1];

		if (label == ".text" || label == ".data" || label == ".bss") {
			if (lineTokens.size() != 2)
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Redundant symbols.");
		} else {
			flags = 0;
			if (lineTokens.size() > 3)
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Redundant symbols.");
			if (lineTokens.size() == 3){
				if (lineTokens[2].find("r") != string::npos)
					flags += 8;
				if (lineTokens[2].find("w") != string::npos)
					flags += 4;
				if (lineTokens[2].find("x") != string::npos)
					flags += 2;
				if (lineTokens[2].find("p") != string::npos)
					flags += 1;
			}
		}
	} else if (lineTokens[0] == ".text" || lineTokens[0] == ".data" || lineTokens[0] == ".bss"){
		if (lineTokens.size() != 1)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Redundant symbols.");
		section = lineTokens[0].substr(1, lineTokens[0].length() - 1);
		label = lineTokens[0];
	} else
		return false;

	if (label == ".text")
		flags = 15;
	else if (label == ".data")
		flags = 13;
	else if (label == ".bss")
		flags = 12;

	if (firstPass) {
		if (checkIfSymbolAlreadyExists(label)){
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Section already exists.");
		} else {
			Assembler::tableOfSymbols.findBySerialNumber(serialNumForSections-1);
			((Symbol*) Assembler::tableOfSymbols.getCurrentData())->size = locationCounter;
			locationCounter = 0;
			if (!isCorrectName(section))
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Name should only contain letters, numbers and characters '_'. In first position it should be a letter.");
			Symbol* symb = new Symbol(label, section, locationCounter, "local", serialNumForSections++);
			symb->flags = flags;
			Assembler::tableOfSymbols.add(symb, symb->serialNum);
		}
	} else {
		locationCounter = 0;
		indexOfContainer++;
		vector<string> helper;
		Assembler::container.push_back(helper);
	}
	return true;
}

bool Parser::doLabel(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass) {
	if (lineTokens[0][lineTokens[0].length() - 1] == ':') {
		string label = lineTokens[0].substr(0, lineTokens[0].length() - 1);

		if (firstPass) {
			if (checkIfEQUSimbolAlreadyExists(lineTokens[0]) != -1)
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Symbol already exists.");
			if (checkIfHelperSymbolAlreadyExists(label)){
				Symbol* temp = (Symbol*) Assembler::tableOfSymbolsWithoutSections.getCurrentData();
				if (temp->section != "?")
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Symbol already exists.");
				if (temp->locality == "extern")
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Extern symbol can not be defined.");
				temp->section = section;
				temp->offset = locationCounter;
			} else {
				if (!isCorrectName(label))
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Name should only contain letters, numbers and characters '_'. In first position it should be a letter. It should contain at least one character.");
				Symbol* symb = new Symbol(label, section, locationCounter, "local", serialNumForOthers++);
				Assembler::tableOfSymbolsWithoutSections.add(symb, symb->serialNum);
			}
		}
	} else
		return false;
	return true;
}

bool Parser::doOperation(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass, int indexOfLine) {
	string mnemonic = lineTokens[0];
	bool Wflag = true;
	if (mnemonic != "sub" && mnemonic[mnemonic.length() - 1] == 'b') {
		Wflag = false;
		mnemonic = mnemonic.substr(0, mnemonic.length() - 1);
	} else if (mnemonic[mnemonic.length() - 1] == 'w') {
		mnemonic = mnemonic.substr(0, mnemonic.length() - 1);
	}

	int numOfOperands = returnNumOfOperands(mnemonic);
	if (numOfOperands == -1)
		return false;
	if ((flags & 0x02) == 0)
		throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: In this section, no operations must be defined.");
	if (numOfOperands != (lineTokens.size() - 1))
		throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Incorrect number of operands.");

	int operationCode = 0;

	if (mnemonic == "halt")
		operationCode = 1*8;
	else if (mnemonic == "xchg")
		operationCode = 2*8;
	else if (mnemonic == "int")
		operationCode = 3*8;
	else if (mnemonic == "mov")
		operationCode = 4*8;
	else if (mnemonic == "add")
		operationCode = 5*8;
	else if (mnemonic == "sub")
		operationCode = 6*8;
	else if (mnemonic == "mul")
		operationCode = 7*8;
	else if (mnemonic == "div")
		operationCode = 8*8;
	else if (mnemonic == "cmp")
		operationCode = 9*8;
	else if (mnemonic == "not")
		operationCode = 10*8;
	else if (mnemonic == "and")
		operationCode = 11*8;
	else if (mnemonic == "or")
		operationCode = 12*8;
	else if (mnemonic == "xor")
		operationCode = 13*8;
	else if (mnemonic == "test")
		operationCode = 14*8;
	else if (mnemonic == "shl")
		operationCode = 15*8;
	else if (mnemonic == "shr")
		operationCode = 16*8;
	else if (mnemonic == "push")
		operationCode = 17*8;
	else if (mnemonic == "pop")
		operationCode = 18*8;
	else if (mnemonic == "jmp")
		operationCode = 19*8;
	else if (mnemonic == "jeq")
		operationCode = 20*8;
	else if (mnemonic == "jne")
		operationCode = 21*8;
	else if (mnemonic == "jgt")
		operationCode = 22*8;
	else if (mnemonic == "call")
		operationCode = 23*8;
	else if (mnemonic == "ret")
		operationCode = 24*8;
	else if (mnemonic == "iret")
		operationCode = 25*8;

	if (Wflag) operationCode += 4;
	if (!firstPass) Assembler::container[indexOfContainer].push_back(intToHex(operationCode));
	locationCounter++;

	if (numOfOperands == 1){
		if (mnemonic == "push")
			doAddressingMode(lineTokens[1], locationCounter, numOfLine, firstPass, Wflag, false, indexOfLine);
		else
			doAddressingMode(lineTokens[1], locationCounter, numOfLine, firstPass, Wflag, true, indexOfLine);
	} else if (numOfOperands == 2){
		doAddressingMode(lineTokens[1], locationCounter, numOfLine, firstPass, Wflag, true, indexOfLine);

		if (mnemonic == "xchg")
			doAddressingMode(lineTokens[2], locationCounter, numOfLine, firstPass, Wflag, true, indexOfLine);
		else
			doAddressingMode(lineTokens[2], locationCounter, numOfLine, firstPass, Wflag, false, indexOfLine);
	}
	return true;
}

bool Parser::doDirective(vector<string>& lineTokens, int& locationCounter, int numOfLine, bool firstPass) {
	if (lineTokens[0] == ".equ") {
		if (lineTokens.size() != 3)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		if (firstPass) {
			if (checkIfHelperSymbolAlreadyExists(lineTokens[1]) || checkIfEQUSimbolAlreadyExists(lineTokens[1]) != -1)
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Symbol already exists.");
			if (!isCorrectName(lineTokens[1]))
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Name should only contain letters, numbers and characters '_'. In first position it should be a letter.");
			Assembler::EQUsimbols.push_back(lineTokens[1]);
			Assembler::EQUsimbols.push_back(lineTokens[2]);
		} else {
			if (!isNumber(lineTokens[2]) && !Assembler::tableOfSymbols.findSymbolByLabel(lineTokens[2]))
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Operand must be a number or symbol.");
		}
	} else if (lineTokens[0] == ".global") {
		if (lineTokens.size() < 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		for (unsigned i = 1; i < lineTokens.size(); i++){
			string label = lineTokens[i];
			if (firstPass) {
				if (checkIfEQUSimbolAlreadyExists(label) != -1)
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Symbol defined with .equ cannot be global.");
				if (checkIfHelperSymbolAlreadyExists(label)){
					Symbol* temp = (Symbol*) Assembler::tableOfSymbolsWithoutSections.getCurrentData();
					if (temp->locality != "local")
						throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: This can not be done.");
					temp->locality = "global";
				} else {
					if (!isCorrectName(label))
						throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Name should only contain letters, numbers and characters '_'. In first position it should be a letter. It should contain at least one character.");
					Symbol* symb = new Symbol(label, "?", 0, "global", serialNumForOthers++);
					Assembler::tableOfSymbolsWithoutSections.add(symb, symb->serialNum);
				}
			} else {
				checkIfSymbolAlreadyExists(label);
				Symbol* temp = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
				if (temp->section == "?" && temp->locality != "extern")
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Global symbol is not defined correctly.");
			}
		}
	} else if (lineTokens[0] == ".extern") {
		if (lineTokens.size() < 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		for (unsigned i = 1; i < lineTokens.size(); i++){
			string label = lineTokens[i];
			if (firstPass) {
				if (checkIfSymbolAlreadyExists(label) || checkIfHelperSymbolAlreadyExists(label) || checkIfEQUSimbolAlreadyExists(label) != -1)
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Symbol already defined.");
				if (!isCorrectName(label))
					throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Name should only contain letters, numbers and characters '_'. In first position it should be a letter.");
				Symbol* symb = new Symbol(label, "?" , 0, "extern", serialNumForOthers++);
				Assembler::tableOfSymbolsWithoutSections.add(symb, symb->serialNum);
			}
		}
	} else if (lineTokens[0] == ".byte") {
		if (lineTokens.size() != 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");

		if (!firstPass){
		int indexOfEQUsimbol = checkIfEQUSimbolAlreadyExists(lineTokens[1]);
		if (isNumber(lineTokens[1])){
			Assembler::container[indexOfContainer].push_back(intToHex(strToInt(lineTokens[1]) & 0xff));
		} else if (indexOfEQUsimbol != -1 && isNumber(Assembler::EQUsimbols[indexOfEQUsimbol])){
			Assembler::container[indexOfContainer].push_back(intToHex(strToInt(Assembler::EQUsimbols[indexOfEQUsimbol]) & 0xff));
		} else
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Operand must be a number.");
		}
		locationCounter++;
	} else if (lineTokens[0] == ".word") {
		if (lineTokens.size() != 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		if (!firstPass){
			bool numOk = false;
			int num;
			Symbol* symb;
			int indexOfEQUsimbol = checkIfEQUSimbolAlreadyExists(lineTokens[1]);

			if (isNumber(lineTokens[1])){
				num = strToInt(lineTokens[1]);
				numOk = true;
			} else if (indexOfEQUsimbol != -1 && isNumber(Assembler::EQUsimbols[indexOfEQUsimbol])) {
				num = strToInt(Assembler::EQUsimbols[indexOfEQUsimbol]);
				numOk = true;
			} else if (Assembler::tableOfSymbols.findSymbolByLabel(lineTokens[1])){
				symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
			} else if (indexOfEQUsimbol != -1 && Assembler::tableOfSymbols.findSymbolByLabel(Assembler::EQUsimbols[indexOfEQUsimbol])) {
				symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
			} else
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Operand must be a number or symbol.");

			if (numOk){
				Assembler::container[indexOfContainer].push_back(intToHex(num & 0xff));
				Assembler::container[indexOfContainer].push_back(intToHex((num>>8) & 0xff));
			} else {
				Relocation* record;
				if (symb->locality == "local"){
					Symbol* symbSect = getSectionSymbolForSymbol(symb);
					record = new Relocation(section, locationCounter, "R_386_16", symbSect->serialNum);
					Assembler::container[indexOfContainer].push_back(intToHex(symb->offset & 0xff));
					Assembler::container[indexOfContainer].push_back(intToHex((symb->offset >>8) & 0xff));
				} else {
					record = new Relocation(section, locationCounter, "R_386_16", symb->serialNum);
					Assembler::container[indexOfContainer].push_back(intToHex(0 & 0xff)); //symb->offset
					Assembler::container[indexOfContainer].push_back(intToHex((0 >>8) & 0xff));
				}
				Assembler::relocationRecords.add(record, record->serialNum);
			}
		}
		locationCounter += 2;
	} else if (lineTokens[0] == ".align") {
		if (lineTokens.size() != 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		if (!isNumber(lineTokens[1]))
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Operand must be a number.");
		while ((locationCounter % strToInt(lineTokens[1])) != 0){
			if (!firstPass)
				Assembler::container[indexOfContainer].push_back(intToHex(0));
			locationCounter++;
		}
	} else if (lineTokens[0] == ".skip") {
		if (lineTokens.size() != 2)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Invalid number of arguments.");
		if (!isNumber(lineTokens[1]))
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Operand must be a number.");
		if (!firstPass){
			for (int i = 0; i < strToInt(lineTokens[1]); i++)
				Assembler::container[indexOfContainer].push_back(intToHex(0));
		}
		locationCounter += strToInt(lineTokens[1]);
	} else if (lineTokens[0] == ".end") {

	} else {
		return false;
	}
	return true;
}

void Parser::doAddressingMode(string token, int& locationCounter, int numOfLine, bool firstPass, bool Wflag, bool dst, int indexOfLine){
	if (isNumber(token)) {//immed value
		if (dst)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing the destionation operand.");
		doImmedValue(token, locationCounter, firstPass, Wflag);

	} else if (token[0] == '&' && isCorrectNameForSymbol(token.substr(1, token.length()-1))){//immed symbol
		if (dst)
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing the destionation operand.");
		if (!doImmedSymbol(token.substr(1, token.length()-1), locationCounter, firstPass, Wflag))
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");

	} else if ((token[0] == 'r' && isdigit(token[1]) && (token[1] - '0') < 8) || token.substr(0, 2) == "sp" || token.substr(0, 2) == "pc" || token.substr(0, 3) == "psw") {
		int numOfReg;
		if (token.substr(0, 2) == "sp")	numOfReg = 6;
		else if (token.substr(0, 2) == "pc") numOfReg = 7;
		else if (token.substr(0, 3) == "psw") numOfReg = 15;
		else numOfReg = strToInt(string(1, token[1]));

		string temp;
		if (token.substr(0, 3) == "psw") temp = token.substr(3, token.length()-3);
		else temp = token.substr(2, token.length()-2);

		if (temp.length() == 0 || temp == "h" || temp =="l"){//regdir
			doRegDir(locationCounter, firstPass, Wflag, numOfReg, temp);

		} else if (temp[0] == '[' && temp[temp.length()-1] == ']' && isNumber(temp.substr(1, temp.length() - 2))){//regind value
			doRegIndValue(locationCounter, firstPass, numOfReg, temp.substr(1, temp.length() - 2));

		} else if (temp[0] == '[' && temp[temp.length()-1] == ']' && isCorrectNameForSymbol(temp.substr(1, temp.length() - 2))){//regind symbol
			if (!doRegIndSymbol(locationCounter, firstPass, numOfReg, temp.substr(1, temp.length() - 2), indexOfLine))
				throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");

		} else
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");
	} else if (token[0] == '$' && isCorrectNameForSymbol(token.substr(1, token.length()-1))) {//pcrel symbol
		if (!doRegIndSymbol(locationCounter, firstPass, 7, token.substr(1, token.length()-1), indexOfLine))
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");

	} else if (isCorrectNameForSymbol(token)) {//abs symbol
		if (!doAbsSymbol(locationCounter, firstPass, token))
			throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");

	} else if (token[0] == '*' && isNumber(token.substr(1, token.length()-1))) {//abs value
		doAbsValue(locationCounter, firstPass, token.substr(1, token.length()-1));

	} else
		throw runtime_error("Line " + intToStr(numOfLine) + ": Syntax error: Wrong way of addressing operand.");
}

void Parser::doImmedValue(string token, int& locationCounter, bool firstPass, bool Wflag){
	if (!firstPass) {
		Assembler::container[indexOfContainer].push_back(intToHex(0));
		Assembler::container[indexOfContainer].push_back(intToHex(strToInt(token) & 0xff));
		if (Wflag) Assembler::container[indexOfContainer].push_back(intToHex((strToInt(token)>>8) & 0xff));
	}
	if (Wflag) locationCounter += 3;
	else locationCounter += 2;
}

bool Parser::doImmedSymbol(string name, int& locationCounter, bool firstPass, bool Wflag){
	if (firstPass){
		if (Wflag) locationCounter += 3;
		else locationCounter += 2;
	} else {
		int indexOfEQUsimbol = checkIfEQUSimbolAlreadyExists(name);
		Symbol* symb;
		if (Assembler::tableOfSymbols.findSymbolByLabel(name))
			symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
		else if (indexOfEQUsimbol != -1 && Assembler::tableOfSymbols.findSymbolByLabel(Assembler::EQUsimbols[indexOfEQUsimbol]))
			symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
		else if (indexOfEQUsimbol != -1 && isNumber(Assembler::EQUsimbols[indexOfEQUsimbol])){
			doImmedValue(Assembler::EQUsimbols[indexOfEQUsimbol], locationCounter, firstPass, Wflag);
			return true;
		} else
			return false;

		Assembler::container[indexOfContainer].push_back(intToHex(0));
		locationCounter++;
		Relocation* record;
		if (symb->locality == "local"){
			Symbol* symbSect = getSectionSymbolForSymbol(symb);
			record = new Relocation(section, locationCounter, "R_386_16", symbSect->serialNum);
			Assembler::container[indexOfContainer].push_back(intToHex(symb->offset & 0xff));
			if (Wflag) Assembler::container[indexOfContainer].push_back(intToHex((symb->offset >>8) & 0xff));
		} else {
			record = new Relocation(section, locationCounter, "R_386_16", symb->serialNum);
			Assembler::container[indexOfContainer].push_back(intToHex(0 & 0xff));//symb->offset
			if (Wflag) Assembler::container[indexOfContainer].push_back(intToHex((0 >>8) & 0xff));
		}
		Assembler::relocationRecords.add(record, record->serialNum);
		if (Wflag) locationCounter += 2;
		else locationCounter++;
	}
	return true;
}

void Parser::doRegDir(int& locationCounter, bool firstPass, bool Wflag, int numOfReg, string high){
	int descr = (1*32) + (numOfReg*2);
	if (!Wflag && high == "h") descr += 1;
	if (!firstPass) Assembler::container[indexOfContainer].push_back(intToHex(descr));
	locationCounter++;
}

void Parser::doRegIndValue(int& locationCounter, bool firstPass, int numOfReg, string num){
	int displacement = strToInt(num);
	if (displacement == 0){
		int descr = (2*32) + (numOfReg*2);
		if (!firstPass) Assembler::container[indexOfContainer].push_back(intToHex(descr));
		locationCounter++;

	} else {
		int descr;
		if (displacement>128 || displacement<=(-128)) descr = 4*32;
		else descr = 3*32;
		descr += numOfReg*2;
		if (!firstPass) {
			Assembler::container[indexOfContainer].push_back(intToHex(descr));
			Assembler::container[indexOfContainer].push_back(intToHex(displacement & 0xff));
			if ((descr & 0x0e0) == (4*32))
				Assembler::container[indexOfContainer].push_back(intToHex((displacement>>8) & 0xff));
		}
		if ((descr & 0x0e0) == (4*32)) locationCounter += 3;
		else locationCounter += 2;
	}
}

bool Parser::doRegIndSymbol(int& locationCounter, bool firstPass, int numOfReg, string name, int indexOfLine){
	if (firstPass){
		locationCounter += 3;
		return true;
	}
	int descr = (4*32) + (numOfReg*2);
	Assembler::container[indexOfContainer].push_back(intToHex(descr));
	locationCounter++;
	int indexOfEQUsimbol = checkIfEQUSimbolAlreadyExists(name);
	Symbol* symb;
	if (Assembler::tableOfSymbols.findSymbolByLabel(name))
		symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
	else if (indexOfEQUsimbol != -1 && Assembler::tableOfSymbols.findSymbolByLabel(Assembler::EQUsimbols[indexOfEQUsimbol]))
		symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
	else if (indexOfEQUsimbol != -1 && isNumber(Assembler::EQUsimbols[indexOfEQUsimbol])){
		Assembler::container[indexOfContainer].push_back(intToHex(strToInt(Assembler::EQUsimbols[indexOfEQUsimbol]) & 0xff));
		Assembler::container[indexOfContainer].push_back(intToHex((strToInt(Assembler::EQUsimbols[indexOfEQUsimbol])>>8) & 0xff));
		locationCounter += 2;
		return true;
	} else
		return false;

	int displacement;
	if (symb->locality == "local"){
		if (section == symb->section){
			displacement = symb->offset - getNextOperationLocation(indexOfLine);

		} else {
			Symbol* symbSect = getSectionSymbolForSymbol(symb);
			Relocation* record = new Relocation(section, locationCounter, "R_386_PC16", symbSect->serialNum);
			Assembler::relocationRecords.add(record, record->serialNum);
			displacement = -(getNextOperationLocation(indexOfLine)-locationCounter) + symb->offset;
		}

	} else {
		Relocation* record = new Relocation(section, locationCounter, "R_386_PC16", symb->serialNum);
		Assembler::relocationRecords.add(record, record->serialNum);
		displacement = -(getNextOperationLocation(indexOfLine)-locationCounter);
	}
	Assembler::container[indexOfContainer].push_back(intToHex(displacement & 0xff));
	Assembler::container[indexOfContainer].push_back(intToHex((displacement >>8) & 0xff));
	locationCounter += 2;
	return true;
}

bool Parser::doAbsSymbol(int& locationCounter, bool firstPass, string name){
	if (firstPass){
		locationCounter += 3;
		return true;
	}
	int indexOfEQUsimbol = checkIfEQUSimbolAlreadyExists(name);
	Symbol* symb;
	if (Assembler::tableOfSymbols.findSymbolByLabel(name))
		symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
	else if (indexOfEQUsimbol != -1 && Assembler::tableOfSymbols.findSymbolByLabel(Assembler::EQUsimbols[indexOfEQUsimbol]))
		symb = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
	else if (indexOfEQUsimbol != -1 && isNumber(Assembler::EQUsimbols[indexOfEQUsimbol])){
		doAbsValue(locationCounter, firstPass, Assembler::EQUsimbols[indexOfEQUsimbol]);
		return true;
	} else
		return false;

	Assembler::container[indexOfContainer].push_back(intToHex(5*32));
	locationCounter++;
	Relocation* record;
	if (symb->locality == "local"){
		Symbol* symbSect = getSectionSymbolForSymbol(symb);
		record = new Relocation(section, locationCounter, "R_386_16", symbSect->serialNum);
		Assembler::container[indexOfContainer].push_back(intToHex(symb->offset & 0xff));
		Assembler::container[indexOfContainer].push_back(intToHex((symb->offset >>8) & 0xff));
	} else {
		record = new Relocation(section, locationCounter, "R_386_16", symb->serialNum);
		Assembler::container[indexOfContainer].push_back(intToHex(0 & 0xff));//symb->offset
		Assembler::container[indexOfContainer].push_back(intToHex((0 >>8) & 0xff));
	}
	Assembler::relocationRecords.add(record, record->serialNum);
	locationCounter += 2;
	return true;
}

void Parser::doAbsValue(int& locationCounter, bool firstPass, string num){
	if (!firstPass) {
		Assembler::container[indexOfContainer].push_back(intToHex(5*32));
		Assembler::container[indexOfContainer].push_back(intToHex(strToInt(num) & 0xff));
		Assembler::container[indexOfContainer].push_back(intToHex((strToInt(num)>>8) & 0xff));
	}
	locationCounter += 3;
}

int Parser::returnNumOfOperands(string name) {
	if (name == "halt" || name == "ret" || name == "iret") {
		return 0;
	} else if (name == "int" || name == "not" || name == "push" || name == "pop"
			|| name == "jmp" || name == "jeq" || name == "jne" || name == "jgt"
			|| name == "call") {
		return 1;
	} else if (name == "xchg" || name == "mov" || name == "add" || name == "sub"
			|| name == "mul" || name == "div" || name == "cmp" || name == "and"
			|| name == "or" || name == "xor" || name == "test" || name == "shl"
			|| name == "shr") {
		return 2;
	} else
		return -1;
}

bool Parser::checkIfSymbolAlreadyExists(string label){
	for (Assembler::tableOfSymbols.onFirst(); Assembler::tableOfSymbols.isCurrent(); Assembler::tableOfSymbols.onNext()) {
		Symbol* temp = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
		if (temp->label == label)
			return true;
	}
	return false;
}

bool Parser::checkIfHelperSymbolAlreadyExists(string label){
	for (Assembler::tableOfSymbolsWithoutSections.onFirst(); Assembler::tableOfSymbolsWithoutSections.isCurrent(); Assembler::tableOfSymbolsWithoutSections.onNext()){
		Symbol* temp = (Symbol*) Assembler::tableOfSymbolsWithoutSections.getCurrentData();
		if (temp->label == label)
			return true;
	}
	return false;
}

int Parser::checkIfEQUSimbolAlreadyExists(string label){
	for (unsigned i = 0; i < Assembler::EQUsimbols.size(); i += 2)
		if (Assembler::EQUsimbols[i] == label) return (i+1);
	return -1;
}

bool Parser::isCorrectNameForSymbol(string name){
	if (isCorrectName(name) || (name[0] == '.' && isCorrectName(name.substr(1, name.length()-1))) || name == "?") return true;
	return false;
}

int Parser::getNextOperationLocation(int indexOfLine){
	int nextOperationLocation = locationCounters[indexOfLine+1];
	if (nextOperationLocation == 0){
		Assembler::tableOfSymbols.findSymbolByLabel("." + section);
		Symbol* symbSect = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
		nextOperationLocation = symbSect->size;
	}
	return nextOperationLocation;
}

Symbol* Parser::getSectionSymbolForSymbol(Symbol* symb){
	Symbol* symbSect;
	if (symb->label[0] == '.') symbSect = symb;
	else {
		Assembler::tableOfSymbols.findSymbolByLabel("." + symb->section);
		symbSect = (Symbol*) Assembler::tableOfSymbols.getCurrentData();
	}
	return symbSect;
}

bool Parser::isNumber(string name) {
	if (name.length() == 0) return false;
	for (unsigned i = 0; i < name.length(); i++)
		if (!isdigit(name[i])) return false;
	return true;
}

bool Parser::isCorrectName(string name) {
	if (name.length() == 0) return false;
	for (unsigned i = 0; i < name.length(); i++)
		if (!isalnum(name[i]) && name[i] != '_') return false;
	if (isdigit(name[0]) || name[0] == '_') return false;
	return true;
}

string Parser::intToHex(int i) {
	stringstream stream;
	stream << "0x" << setfill ('0') << setw(2) << hex << i;
	return stream.str();
}

string Parser::intToStr(int i) {
	stringstream s;
	s << i;
	return s.str();
}

int Parser::strToInt(string s){
	int i;
	istringstream (s) >> i;
	return i;
}
