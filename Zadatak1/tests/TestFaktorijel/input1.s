.global main
.global func
.extern fact, res
.text
main:	mov r3, 8
	mov r4, &array
	add r4, &disp
	mov r5, &loop
loop:
	push r3
	call func
	pop r3
	mov r4[0], res
	sub r4, 2
	sub r3, 1
	cmp r3, 0
	jne r5
	halt
.data
.equ disp, 14
.equ factorial, fact
func: .word factorial
.bss
array: .skip 16
.end
