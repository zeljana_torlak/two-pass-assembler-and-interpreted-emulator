.global fact, res
.extern func
.text
fact:	mov r1, &skip
	cmp sp[1], 0
	jgt r1
	mov res, 1
	ret
skip: mov r0, sp[1]
	sub r0, 1
	push r0
	call func
	pop r0
	mul res, sp[1]
	ret
.data
res:	.word 0
.end
