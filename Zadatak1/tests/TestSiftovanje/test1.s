.global main, res
.extern calc

.section .text
main:
xchgw *65296, timer
orb pswh, $a
call calcf
halt

.data
a: .byte 64
res: .word 0
timer: .word 4
calcf: .word calc
.end
