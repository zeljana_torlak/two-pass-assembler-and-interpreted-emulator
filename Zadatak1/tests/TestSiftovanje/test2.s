.global calc
.extern res
.section .test rwxp
calc:
movb r5l, &val
orb r5h, 1
movw r4, adr
sub r4, 2
mov r0, &skip

loop:
shr r5, 1
mov r4[2], r5
add res, r4[skip]
jmp r0
skip:
add r4, 2
mov r3, r5
not r3
test r3, 1
jeq l
ret

.data
.equ val, 255
l: .word loop
adr: .word .bss

.bss
.skip 1
.align 16
.end
