#ifndef LINKER_H_
#define LINKER_H_

#include "Table.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
class Symbol;
class Relocation;

class Linker {
public:
	Linker();
	~Linker();

	vector<vector<vector<string> > > tokens;

	void reset();
	void initialize(ifstream& inputFile);
	void link();
	void checkIfExistsUnresolvedSymbolsOrMultipleDefinitions();

	vector<string> parseInputFile(const string &text);
	bool isLetter(string name);
	bool isNumber(string name);
	bool isCorrectName(string name);
	int strToInt(string s);

	static vector<Table*> tableOfSymbols;
	static vector<Table*> relocationRecords;
	static vector<vector<vector<string> > > container;

};

#endif
