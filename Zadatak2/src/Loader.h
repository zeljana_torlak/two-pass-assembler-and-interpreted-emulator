#ifndef LOADER_H_
#define LOADER_H_

#include <vector>
#include <string>
using namespace std;
class Symbol;
class Relocation;

class Loader {
public:
	Loader();
	~Loader();

	void load();
	void checkIfExistsDuplicateInSectionPlaces();
	void definePositions(unsigned numOfFile, string section, int beginOfSection);
	void resolveMain();
	void resolve();

	string intToHex(int i);
	int hexToInt(string s);

	static vector<string> sectionPlaces;

};

#endif
