#include "Linker.h"
#include "Loader.h"
#include "Processor.h"
#include <stdlib.h> //exit
using namespace std;

int main(int argc, char* argv[]) {

	vector<string> args;
	for (int i = 2; i < argc; i++)
		args.push_back(string(argv[i]));

	Processor processor;
	Linker linker;
	Loader loader;

	for (unsigned i = 0; i < args.size(); i++){
		if (args[i][args[i].length()-1] == 'o' && args[i][args[i].length()-2] == '.'){
			ifstream inputFile;
			inputFile.open(args[i].c_str());
			if (!inputFile.is_open()) {
				cerr << "Error while opening file: " << args[i] << endl;
				exit(1);
			}
			try {
				linker.initialize(inputFile);
			} catch (const exception& e) {
				cerr << e.what() << "\nFatal error" << endl;
			}
			inputFile.close();
		} else if ( (args[i].substr(0, 7)) == "-place=") {
			string temp = args[i].substr(7, args[i].length()-7);
			if (temp.find("@") != string::npos) {
				int pos = temp.find("@");
				string s1 = temp.substr(0, pos);
				string s2 = temp.substr(pos+1, temp.length()-pos-1);
				Loader::sectionPlaces.push_back(s1);
				Loader::sectionPlaces.push_back(s2);
			} else {
				cerr << "Incorrect use of attribute place." << "\nFatal error" << endl;
				exit(1);
			}
		} else {
			cerr << "Unrecognized command." << "\nFatal error" << endl;
			exit(1);
		}
	}

	try {
		linker.link();
		loader.load();
		processor.execute();
	} catch (const exception& e) {
		cerr << e.what() << "\nFatal error" << endl;
	}

	for (int i = 20480; i < 20496; i++)
		cout << i << ": " << Processor::memory[i] << endl;

	cout << "\nPolar bear" << endl;
	return 0;
}
