#ifndef RELOCATION_H_
#define RELOCATION_H_

#include <iostream>
#include <string>
using namespace std;

class Relocation {
public:
	string section;
	int offset;
	string type;
	int serialNum;

	Relocation(string _section, int _offset, string _type,
			int _serialNum);
	~Relocation();
};

#endif
