#include "Table.h"
#include "Symbol.h"

void Table::clear() {
	while (first) {
		old = first;
		first = first->next;
		delete old;
	}
	last = curr = prev = 0;
	old = 0;
	size = 0;
}

bool Table::findBySerialNumber(int serialNum) {
	if (!isCurrent())
		onFirst();
	else if (serialNum < curr->serialNum)
		onFirst();
	for (; isCurrent() && curr->serialNum < serialNum; onNext())
		;
	if (isCurrent() && curr->serialNum == serialNum)
		return true;
	else
		return false;
}

bool Table::findSymbolByLabel(string label) {
	if (isCurrent() && ((Symbol*) curr->data)->label == label)
		return true;
	for (onFirst(); isCurrent(); onNext())
		if (((Symbol*) curr->data)->label == label)
			return true;
	return false;
}

void Table::onLast() {
	curr = last;
	if (first == last) {
		prev = 0;
	} else {
		prev = first;
		while (prev->next != curr) {
			prev = prev->next;
		}
	}
}
