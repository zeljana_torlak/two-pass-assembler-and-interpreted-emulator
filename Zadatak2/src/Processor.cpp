#include "Processor.h"
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <signal.h>
#include <curses.h>

int Processor::psw = 0;
int Processor::r[8] = {0};
vector<string> Processor::memory;

void handler(int x){
	if ((Processor::psw & 0x8000) == 0 && (Processor::psw & 0x2000) == 0){
		int type;
		string s = Processor::memory[65296].substr(2,  Processor::memory[65296].length()-2);
		istringstream (s) >> type;
		type &= 0x07;
		if (type == 0) alarm(1);
		else if (type == 1) alarm(1);
		else if (type == 2) alarm(1.5);
		else if (type == 3) alarm(2);
		else if (type == 4) alarm(5);
		else if (type == 5) alarm(10);
		else if (type == 6) alarm(30);
		else if (type == 7) alarm(60);
		cout << "timer! ";
	}
}

Processor::Processor() {
	for (int i = 0; i < 65536; i++)
		memory.push_back("?");
	r[6] = 65281;
}

Processor::~Processor() {
	memory.clear();
}

void Processor::execute(){
	bool ok = true;
	initializeMemory();
	signal(SIGALRM, handler);
	alarm(1);

	while (ok){
		checkIfAddressIsCorrect(r[7]);
		int instrCode = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
		r[7]++;

		int oprCode = (instrCode >>3) & 0x1f;
		bool Wflag = ((((instrCode >>2) & 0x01) == 0) ? false : true);

		int operand1, operand2;
		string addressOfOperand1, addressOfOperand2;
		int opr1Code, opr2Code;
		int temp = 0;
		highFlag = false;
		bool highFlag2 = false;

		switch (oprCode) {
			case 1: //halt
				ok = false;
				break;
			case 2: //xchg
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;
				if (highFlag) {
					highFlag2 = true; highFlag = false;
				}

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, true) & 0xffff;

				temp = operand1;
				store(Wflag, operand1, operand2, addressOfOperand1, 0);
				store(Wflag, 0, temp, addressOfOperand2, 0);

				break;
			case 3: //int
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				r[6] -= 2;
				checkIfAddressIsCorrect(r[6]);
				memory[r[6]] = intToHex((psw >>8) & 0xff);
				checkIfAddressIsCorrect(r[6]-1);
				memory[r[6]-1] = intToHex(psw & 0xff);

				checkIfAddressIsCorrect((operand1 % 8)*2);
				checkIfAddressIsCorrect((operand1 % 8)*2 +1);
				r[7] = hexToInt(Processor::memory[(operand1 % 8)*2 +1].substr(2, Processor::memory[(operand1 % 8)*2 +1].length()-2) + Processor::memory[(operand1 % 8)*2].substr(2, Processor::memory[(operand1 % 8)*2].length()-2));

				break;
			case 4: //mov
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 0);
				updatePSWbits(operand2, Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 5: //add
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 1);
				updatePSWbits((operand1 + operand2), Wflag, true, true, true, true, 1, operand1, operand2);
				break;
			case 6: //sub
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 2);
				updatePSWbits((operand1 - operand2), Wflag, true, true, true, true, 2, operand1, operand2);
				break;
			case 7: //mul
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 3);
				updatePSWbits((operand1 * operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 8: //div
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				if (operand2 == 0)
					throw runtime_error("Processor: Division by zero is not possible.");

				store(Wflag, operand1, operand2, addressOfOperand1, 4);
				updatePSWbits((operand1 / operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 9: //cmp
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				temp = (operand1 - operand2) & 0xffff;
				updatePSWbits((operand1 - operand2), Wflag, true, true, true, true, 2, operand1, operand2);
				break;
			case 10: //not
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				store(Wflag, operand1, 0, addressOfOperand1, 5);
				updatePSWbits((~operand1), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 11: //and
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 6);
				updatePSWbits((operand1 & operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 12: //or
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 7);
				updatePSWbits((operand1 | operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 13: //xor
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 8);
				updatePSWbits((operand1 ^ operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 14: //test
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				temp = (operand1 & operand2) & 0xffff;
				updatePSWbits((operand1 & operand2), Wflag, true, false, false, true, 0, 0, 0);
				break;
			case 15: //shl
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 9);
				updatePSWbits((operand1 << operand2), Wflag, true, false, true, true, 3, operand1, operand2);
				break;
			case 16: //shr
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[7]);
				opr2Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand2 = returnOperand(opr2Code, Wflag, addressOfOperand2, false) & 0xffff;

				store(Wflag, operand1, operand2, addressOfOperand1, 10);
				updatePSWbits((operand1 >> operand2), Wflag, true, false, true, true, 4, operand1, operand2);
				break;
			case 17: //push
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, false) & 0xffff;

				r[6] -= 2;
				checkIfAddressIsCorrect(r[6]);
				if (Wflag) memory[r[6]] = intToHex((operand1 >>8) & 0xff);
				checkIfAddressIsCorrect(r[6]-1);
				memory[r[6]-1] = intToHex(operand1 & 0xff);
				break;
			case 18: //pop
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				checkIfAddressIsCorrect(r[6]);
				if (Wflag) temp = (hexToInt(Processor::memory[r[6]].substr(2, Processor::memory[r[6]].length()-2)) & 0xff) << 8;
				checkIfAddressIsCorrect(r[6]-1);
				temp |= (hexToInt(Processor::memory[r[6]-1].substr(2, Processor::memory[r[6]-1].length()-2)) & 0xff);

				store(Wflag, 0, temp, addressOfOperand1, 0);
				r[6] += 2;
				break;
			case 19: //jmp
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				r[7] = operand1;
				break;
			case 20: //jeq
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				if ((psw & 0x01) == 1){
					r[7] = operand1;
				}

				break;
			case 21: //jne
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				if ((psw & 0x01) == 0){
					r[7] = operand1;
				}

				break;
			case 22: //jgt
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				if (((psw & 0x01) == 0) && (((psw >>1) & 0x01) == ((psw >>3) & 0x01))){
					r[7] = operand1;
				}

				break;
			case 23: //call
				checkIfAddressIsCorrect(r[7]);
				opr1Code = hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
				r[7]++;
				operand1 = returnOperand(opr1Code, Wflag, addressOfOperand1, true) & 0xffff;

				r[6] -= 2;
				checkIfAddressIsCorrect(r[6]);
				memory[r[6]] = intToHex((r[7] >>8) & 0xff);
				checkIfAddressIsCorrect(r[6]-1);
				memory[r[6]-1] = intToHex(r[7] & 0xff);

				r[7] = operand1;
				break;
			case 24: //ret
				checkIfAddressIsCorrect(r[6]);
				if (Wflag) temp = (hexToInt(Processor::memory[r[6]].substr(2, Processor::memory[r[6]].length()-2)) & 0xff) << 8;
				checkIfAddressIsCorrect(r[6]-1);
				temp |= (hexToInt(Processor::memory[r[6]-1].substr(2, Processor::memory[r[6]-1].length()-2)) & 0xff);

				r[7] = temp & 0xffff;
				r[6] += 2;
				break;
			case 25: //iret
				checkIfAddressIsCorrect(r[6]);
				if (Wflag) temp = (hexToInt(Processor::memory[r[6]].substr(2, Processor::memory[r[6]].length()-2)) & 0xff) << 8;
				checkIfAddressIsCorrect(r[6]-1);
				temp |= (hexToInt(Processor::memory[r[6]-1].substr(2, Processor::memory[r[6]-1].length()-2)) & 0xff);

				psw = temp & 0xffff;
				r[6] += 2;

				temp = 0;
				checkIfAddressIsCorrect(r[6]);
				if (Wflag) temp = (hexToInt(Processor::memory[r[6]].substr(2, Processor::memory[r[6]].length()-2)) & 0xff) << 8;
				checkIfAddressIsCorrect(r[6]-1);
				temp |= (hexToInt(Processor::memory[r[6]-1].substr(2, Processor::memory[r[6]-1].length()-2)) & 0xff);

				r[7] = temp & 0xffff;
				r[6] += 2;
				break;
			default:
				throw runtime_error("Processor: Invalid instruction code.");
		}

	}

	//terminal
	if ((Processor::psw & 0x8000) == 0 && (Processor::psw & 0x4000) == 0){
		initscr();
		cbreak();
		nodelay(stdscr, TRUE);
		while(true){
			char x = ERR;
			while (x == ERR){
				refresh();
				x = getch();
			}
			cout << x;
			if (x == '1') break;
			memory[65282] = intToHex(x & 0xff);
			memory[65283] = intToHex((x>>8) & 0xff);
		}
		endwin();
	}
}

void Processor::initializeMemory(){
	for (int i = 0; i < 65536; i++)
		if (memory[i] == "?")
			memory[i] = "0x00";
}

int Processor::returnOperand(int code, bool Wflag, string& addressOfOperand, bool dst){
	int addressingMode = (code >>5) & 0x07;
	int numOfRegister = (code >>1) & 0x0f;
	bool high = (((code & 0x01) == 0) ? false : true);

	string operand;
	int temp;
	string s = "";

	switch (addressingMode) {
		case 0: //immed
			if (dst)
				throw runtime_error("Processor: Wrong way of addressing the destionation operand.");
			addressOfOperand = "#";
			checkIfAddressIsCorrect(r[7]);
			operand = Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2);
			r[7]++;
			if (Wflag){
				checkIfAddressIsCorrect(r[7]);
				operand = Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2) + operand;
				r[7]++;
			}
			break;
		case 1: //regdir
			if (dst && high) highFlag = true;
			if (numOfRegister >= 8 && numOfRegister < 15)
				throw runtime_error("Processor: Invalid number of register.");

			if (numOfRegister != 15){
				addressOfOperand = "r" + intToStr(numOfRegister);
				if (Wflag) return r[numOfRegister];
				else {
					if (high) return (r[numOfRegister] >>8) & 0xff;
					else return r[numOfRegister] & 0xff;
				}
			} else {
				addressOfOperand = "psw";
				if (Wflag) return psw;
				else {
					if (high) return (psw >>8) & 0xff;
					else return psw & 0xff;
				}
			}
			break;
		case 2: //regind without displacement
			if (numOfRegister >= 8 && numOfRegister < 15)
				throw runtime_error("Processor: Invalid number of register.");

			if (numOfRegister != 15) temp = r[numOfRegister];
			else temp = psw;
			addressOfOperand = intToStr(temp);
			checkIfAddressIsCorrect(temp);
			operand = Processor::memory[temp].substr(2, Processor::memory[temp].length()-2);
			if (Wflag) {
				checkIfAddressIsCorrect(temp+1);
				operand = Processor::memory[temp+1].substr(2, Processor::memory[temp+1].length()-2) + operand;
			}
			break;
		case 3: //regind with 8-bit displacement
			if (numOfRegister >= 8 && numOfRegister < 15)
				throw runtime_error("Processor: Invalid number of register.");

			if (numOfRegister != 15) temp = r[numOfRegister];
			else temp = psw;
			checkIfAddressIsCorrect(r[7]);
			temp += hexToInt(Processor::memory[r[7]].substr(2, Processor::memory[r[7]].length()-2));
			r[7]++;
			addressOfOperand = intToStr(temp);
			checkIfAddressIsCorrect(temp);
			operand = Processor::memory[temp].substr(2, Processor::memory[temp].length()-2);
			if (Wflag) {
				checkIfAddressIsCorrect(temp+1);
				operand = Processor::memory[temp+1].substr(2, Processor::memory[temp+1].length()-2) + operand;
			}
			break;
		case 4: //regind with 16-bit displacement
			if (numOfRegister >= 8 && numOfRegister < 15)
				throw runtime_error("Processor: Invalid number of register.");

			checkIfAddressIsCorrect(r[7]);
			r[7]++;
			checkIfAddressIsCorrect(r[7]);
			r[7]++;
			s = Processor::memory[r[7]-1].substr(2, Processor::memory[r[7]-1].length()-2) + Processor::memory[r[7]-2].substr(2, Processor::memory[r[7]-2].length()-2);
			if (s.length() < 8 && (s[0] == 'f' || s[0] == 'e' || s[0] == 'd' || s[0] == 'c' ||
				s[0] == 'b' || s[0] == 'a' || s[0] == '9' || s[0] == '8'))
			while (s.length() < 8)
				s = 'f' + s;
			temp = hexToInt(s);
			if (numOfRegister != 15) temp += r[numOfRegister];
			else temp += psw;

			addressOfOperand = intToStr(temp);
			checkIfAddressIsCorrect(temp);
			operand = Processor::memory[temp].substr(2, Processor::memory[temp].length()-2);
			if (Wflag) {
				checkIfAddressIsCorrect(temp+1);
				operand = Processor::memory[temp+1].substr(2, Processor::memory[temp+1].length()-2) + operand;
			}
			break;
		case 5: //memdir
			checkIfAddressIsCorrect(r[7]);
			r[7]++;
			checkIfAddressIsCorrect(r[7]);
			r[7]++;
			temp = hexToInt(Processor::memory[r[7]-1].substr(2, Processor::memory[r[7]-1].length()-2) + Processor::memory[r[7]-2].substr(2, Processor::memory[r[7]-2].length()-2));
			addressOfOperand = intToStr(temp);
			checkIfAddressIsCorrect(temp);
			operand = Processor::memory[temp].substr(2, Processor::memory[temp].length()-2);
			if (Wflag) {
				checkIfAddressIsCorrect(temp+1);
				operand = Processor::memory[temp+1].substr(2, Processor::memory[temp+1].length()-2) + operand;
			}
			break;
		default:
			throw runtime_error("Processor: Invalid addressing mode.");
	}
	return hexToInt(operand);
}

void Processor::updatePSWbits(int temp, bool Wflag, bool okZ, bool okO, bool okC, bool okN, int type, int op1, int op2){
	if (okZ){
		int Z;
		if (Wflag){
			if ((temp & 0xffff) == 0) Z = 1;
			else Z = 0;
		} else {
			if ((temp & 0x0ff) == 0) Z = 1;
			else Z = 0;
		}
		psw = (psw & 0xfffe) | Z;
	}
	if (okO){
		int O;
		if (type == 1){ //add
			if (Wflag){
				if (((temp & 0x8000) == 0 && (op1 & 0x8000) == 1 && (op2 & 0x8000) == 1) ||
						((temp & 0x8000) == 1 && (op1 & 0x8000) == 0 && (op2 & 0x8000) == 0)) O = 2;
				else O = 0;
			} else {
				if (((temp & 0x80) == 0 && (op1 & 0x80) == 1 && (op2 & 0x80) == 1) ||
						((temp & 0x80) == 1 && (op1 & 0x80) == 0 && (op2 & 0x80) == 0)) O = 2;
				else O = 0;
			}
		}
		if (type == 2){ //sub
			if (Wflag){
				if (((temp & 0x8000) == 0 && (op1 & 0x8000) == 1 && (op2 & 0x8000) == 0) ||
						((temp & 0x8000) == 1 && (op1 & 0x8000) == 0 && (op2 & 0x8000) == 1)) O = 2;
				else O = 0;
			} else {
				if (((temp & 0x80) == 0 && (op1 & 0x80) == 1 && (op2 & 0x80) == 0) ||
						((temp & 0x80) == 1 && (op1 & 0x80) == 0 && (op2 & 0x80) == 1)) O = 2;
				else O = 0;
			}
		}
		psw = (psw & 0xfffd) | O;
	}
	if (okC){
		int C;
		if (type == 1){ //add
			if (Wflag){
				if ((temp & 0x010000) == 0) C = 0;
				else C = 4;
			} else {
				if ((temp & 0x0100) == 0) C = 0;
				else C = 4;
			}
		}
		if (type == 2){ //sub
			if (Wflag){
				if ((temp & 0x010000) == 0) C = 4;
				else C = 0;
			} else {
				if ((temp & 0x0100) == 0) C = 4;
				else C = 0;
			}
		}
		if (type == 3){ //shl
			if (op2 > 0){
				if (Wflag){
					if ((temp & 0x010000) == 0) C = 0;
					else C = 4;
				} else {
					if ((temp & 0x0100) == 0) C = 0;
					else C = 4;
				}
			} else C = 0;
		}
		if (type == 4){ //shr
			if (op2 > 0){
				int helper = op1 << (op2-1);
				if ((helper & 0x01) == 0) C = 0;
				else C = 4;
			} else C = 0;
		}
		psw = (psw & 0xfffb) | C;
	}
	if (okN){
		int N;
		if (Wflag){
			if ((temp & 0x8000) == 0) N = 0;
			else N = 8;
		} else {
			if ((temp & 0x080) == 0) N = 0;
			else N = 8;
		}
		psw = (psw & 0xfff7) | N;
	}
}

void Processor::checkIfAddressIsCorrect(int address){
	if (address < 0 || address > 65536)
		throw runtime_error("Processor: Cannot access specified part of the memory.");

}

string Processor::intToHex(int i) {
	stringstream stream;
	stream << "0x" << setfill ('0') << setw(2) << hex << i;
	return stream.str();
}

int Processor::hexToInt(string s){
	unsigned int x;
	stringstream ss;
	ss << hex << s;
	ss >> x;
	return x;
//	istringstream converter(s);
//	int x;
//	converter >> hex >> x;
//	return x;
}

string Processor::intToStr(int i) {
	stringstream s;
	s << i;
	return s.str();
}

int Processor::strToInt(string s){
	int i;
	istringstream (s) >> i;
	return i;
}

void Processor::store(bool Wflag, int operand1, int operand2, string addressOfOperand1, int typeOfOpr){
	int temp;
	if (typeOfOpr == 0) temp = operand2; //mov
	if (typeOfOpr == 1) temp = operand1 + operand2; //add
	if (typeOfOpr == 2) temp = operand1 - operand2; //sub
	if (typeOfOpr == 3) temp = operand1 * operand2; //mul
	if (typeOfOpr == 4) temp = operand1 / operand2; //mul
	if (typeOfOpr == 5) temp = ~operand1; //not
	if (typeOfOpr == 6) temp = operand1 & operand2; //and
	if (typeOfOpr == 7) temp = operand1 | operand2; //or
	if (typeOfOpr == 8) temp = operand1 ^ operand2; //xor
	if (typeOfOpr == 9) temp = operand1 << operand2; //shl
	if (typeOfOpr == 10) temp = operand1 >> operand2; //shr
	if (addressOfOperand1 == "psw"){
		if (Wflag) psw = temp;
		else if (!highFlag) psw = (psw & 0xff00) | (temp & 0xff);
		else psw = (psw & 0xff) | ((temp<<8) & 0xff00);
	} else if (addressOfOperand1[0] == 'r'){
		int numOfReg = strToInt(addressOfOperand1.substr(1, addressOfOperand1.length()-1));
		if (Wflag) r[numOfReg] = temp;
		else if (!highFlag) r[numOfReg] = (r[numOfReg] & 0xff00) | (temp & 0xff);
		else r[numOfReg] = (r[numOfReg] & 0xff) | ((temp<<8) & 0xff00);
	} else {
		memory[strToInt(addressOfOperand1)] = intToHex(temp & 0xff);
		if (Wflag) memory[strToInt(addressOfOperand1)+1] = intToHex((temp >>8) & 0xff);
	}
}
