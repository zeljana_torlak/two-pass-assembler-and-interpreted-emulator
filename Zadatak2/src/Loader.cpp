#include "Loader.h"
#include "Linker.h"
#include "Processor.h"
#include "Symbol.h"
#include "Relocation.h"
#include <stdexcept>
#include <sstream>
#include <iomanip>

vector<string> Loader::sectionPlaces;

Loader::Loader() {
}

Loader::~Loader() {
}

void Loader::load(){
	checkIfExistsDuplicateInSectionPlaces();

	int position = 16;

	while (sectionPlaces.size() != 0){
		int index = -1, min = -1;
		for (unsigned i = 1; i < sectionPlaces.size(); i += 2){
			int curr = hexToInt(sectionPlaces[i].substr(2, sectionPlaces[i].length()-2));
			if (index == -1 || curr < min){
				index = i; min = curr;
			}
		}

		if (index == -1) break; //never happens
		string section = sectionPlaces[index-1];
		sectionPlaces.erase(sectionPlaces.begin() + index);
		sectionPlaces.erase(sectionPlaces.begin() + index - 1);

		if (min < 0 || min > 65536)
			throw runtime_error("Loader: Cannot access part of the memory specified by attribute place - out of scope.");

		if (section == "iv_table"){


		} else {
			if (min < 16 || min >= 65280)
				throw runtime_error("Loader: Cannot access part of the memory specified by attribute place.");

			position = min;

			for (unsigned i = 0; i < Linker::container.size(); i++){
				for (unsigned j = 0; j < Linker::container[i].size(); j++){
					if (Linker::container[i][j][0] != section) continue;
					Linker::container[i][j][0] = "#" + Linker::container[i][j][0];
					for (unsigned k = 1; k < Linker::container[i][j].size(); k++){
						if (Processor::memory[position] != "?" || position >= 65280)
							throw runtime_error("Loader: Cannot access part of the memory specified by attribute place.");
						Processor::memory[position] = Linker::container[i][j][k];
						position++;
					}
					definePositions(i, section, min);
					break;
				}
				min = position;
			}
		}
	}

	int beginOfSection = position;
	for (unsigned i = 0; i < Linker::container.size(); i++){
		for (unsigned j = 0; j < Linker::container[i].size(); j++){
			if (Linker::container[i][j][0][0] == '#') continue;
			string section = Linker::container[i][j][0];
			Linker::container[i][j][0] = "#" + Linker::container[i][j][0];
			for (unsigned k = 1; k < Linker::container[i][j].size(); k++){
				if (Processor::memory[position] != "?" || position >= 65280)
					throw runtime_error("Loader: Cannot access part of the memory specified by attribute place.");
				Processor::memory[position] = Linker::container[i][j][k];
				position++;
			}
			definePositions(i, section, beginOfSection);
			beginOfSection = position;


			for (unsigned l = 0; l < Linker::container.size(); l++){
				for (unsigned m = 0; m < Linker::container[l].size(); m++){
					if (Linker::container[l][m][0] != section) continue;
					Linker::container[l][m][0] = "#" + Linker::container[l][m][0];
					for (unsigned k = 1; k < Linker::container[l][m].size(); k++){
						if (Processor::memory[position] != "?" || position >= 65280)
							throw runtime_error("Loader: Cannot access part of the memory specified by attribute place.");
						Processor::memory[position] = Linker::container[l][m][k];
						position++;
					}
					definePositions(l, section, beginOfSection);
					break;
				}
				beginOfSection = position;
			}

		}
	}

	resolveMain();
	resolve();
}

void Loader::checkIfExistsDuplicateInSectionPlaces(){
	for (unsigned i = 0; i < sectionPlaces.size(); i+=2){
		string currSection = sectionPlaces[i];
		string currAddress = sectionPlaces[i+1];
		for (unsigned j = i+2; j < sectionPlaces.size(); j+=2){
			if (currSection == sectionPlaces[j])
				throw runtime_error("Loader: Same sections are specified by attribute place.");
			if (currAddress == sectionPlaces[j+1])
				throw runtime_error("Loader: Same addresses for different sections are specified by attribute place.");
		}
	}
}

void Loader::definePositions(unsigned numOfFile, string section, int beginOfSection){
	for (Linker::tableOfSymbols[numOfFile]->onFirst(); Linker::tableOfSymbols[numOfFile]->isCurrent(); Linker::tableOfSymbols[numOfFile]->onNext()){
		Symbol* temp = (Symbol*) Linker::tableOfSymbols[numOfFile]->getCurrentData();
		if (temp->locality == "extern" || temp->section != section) continue;
		temp->position = beginOfSection + temp->offset;
		if (temp->locality == "local") continue;
		for (unsigned i = 0; i < Linker::tableOfSymbols.size(); i++){
			if (i == numOfFile) continue;
			for (Linker::tableOfSymbols[i]->onFirst(); Linker::tableOfSymbols[i]->isCurrent(); Linker::tableOfSymbols[i]->onNext()){
				Symbol* symb = (Symbol*) Linker::tableOfSymbols[i]->getCurrentData();
				if (symb->label == temp->label && symb->locality == "extern")
					symb->position = temp->position;
			}
		}
	}
}

void Loader::resolveMain(){
	for (unsigned i = 0; i < Linker::tableOfSymbols.size(); i++){
		for (Linker::tableOfSymbols[i]->onFirst(); Linker::tableOfSymbols[i]->isCurrent(); Linker::tableOfSymbols[i]->onNext()){
			Symbol* symb = (Symbol*) Linker::tableOfSymbols[i]->getCurrentData();
			if (symb->label == "main" && symb->locality == "global")
				Processor::r[7] = symb->position;
		}
	}
}

void Loader::resolve(){
	for (unsigned i = 0; i<Linker::relocationRecords.size(); i++){
		for (Linker::relocationRecords[i]->onFirst(); Linker::relocationRecords[i]->isCurrent();){
			Relocation* record = (Relocation*) Linker::relocationRecords[i]->getCurrentData();

			if (record->section != "?") Linker::tableOfSymbols[i]->findSymbolByLabel("." + record->section);
			else Linker::tableOfSymbols[i]->findSymbolByLabel(record->section);
			Symbol* sectionSymb = (Symbol*) Linker::tableOfSymbols[i]->getCurrentData();
			int beginOfSection = sectionSymb->position;

			Linker::tableOfSymbols[i]->findBySerialNumber(record->serialNum);
			Symbol* symb = (Symbol*) Linker::tableOfSymbols[i]->getCurrentData();
			if (symb->position == -1) //should never happen
				throw runtime_error("Loader: Error when loading.");

			string temp1 = Processor::memory[beginOfSection+record->offset].substr(2, Processor::memory[beginOfSection+record->offset].length()-2);
			string temp2 = Processor::memory[beginOfSection+record->offset+1].substr(2, Processor::memory[beginOfSection+record->offset+1].length()-2);
			int temp = hexToInt(temp2+temp1);
			int res;
			if (record->type == "R_386_PC16") res = symb->position - beginOfSection - record->offset + temp;
			else res = symb->position + temp;

			Processor::memory[beginOfSection+record->offset] = intToHex(res & 0xff);
			Processor::memory[beginOfSection+record->offset+1] = intToHex((res >>8) & 0xff);

			Linker::relocationRecords[i]->removeCurrent();
		}
	}
}

string Loader::intToHex(int i) {
	stringstream stream;
	stream << "0x" << setfill ('0') << setw(2) << hex << i;
	return stream.str();
}

int Loader::hexToInt(string s){
	istringstream converter(s);
	int x;
	converter >> hex >> x;
	return x;
}
