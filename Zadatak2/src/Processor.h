#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include <vector>
#include <string>
using namespace std;

class Processor {
public:
	Processor();
	~Processor();

	static int psw;
	static int r[];
	static vector<string> memory;

	bool highFlag;

	void execute();
	void initializeMemory();
	int returnOperand(int code, bool Wflag, string& addressOfOperand, bool dst);
	void updatePSWbits(int temp, bool Wflag, bool okZ, bool okO, bool okC, bool okN, int type, int op1, int op2);

	void checkIfAddressIsCorrect(int address);
	string intToHex(int i);
	int hexToInt(string s);
	string intToStr(int i);
	int strToInt(string s);
	bool checkIfSymbolAlreadyExists(string label);
	void store(bool Wflag, int operand1, int operand2, string addressOfOperand1, int typeOfOpr);

};

#endif
