#ifndef TABLE_H_
#define TABLE_H_

#include <iostream>
#include <string>
using namespace std;

class Symbol;

class Table {
public:

	struct Element {
		void* data;
		int serialNum;
		Element* next;
		Element(void*& d, int s, Element* n = 0) {
			data = d;
			serialNum = s;
			next = n;
		}
	};

	Element *first, *last, *curr, *prev;
	Element *old;
	int size;

	Table() {
		first = last = curr = prev = 0;
		old = 0;
		size = 0;
	}

	~Table() {
		clear();
	}

	void add(void* p, int serialNum) { //&
		last = (!first ? first : last->next) = new Element(p, serialNum);
		size++;
	}

	void onFirst() {
		curr = first;
		prev = 0;
	}
	void onNext() {
		prev = curr;
		if (curr)
			curr = curr->next;
	}

	void onLast();

	int isCurrent() const {
		if (curr != 0)
			return 1;
		else
			return 0;
	}

	void*& getCurrentData() { //use only when current!=0
		return curr->data;
	}

	void removeCurrent() {
		if (!curr)
			return;
		old = curr;
		curr = curr->next;
		(!prev ? first : prev->next) = curr;
		if (!curr)
			last = prev;
		delete old;
		size--;
	}

	void clear();

	bool findBySerialNumber(int serialNum);
	bool findSymbolByLabel(string label);
};

#endif
