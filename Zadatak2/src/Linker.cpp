#include "Linker.h"
#include "Symbol.h"
#include "Relocation.h"
#include <stdexcept>
#include <sstream>

vector<Table*> Linker::tableOfSymbols;
vector<Table*> Linker::relocationRecords;
vector<vector<vector<string> > > Linker::container;

Linker::Linker() {
}

Linker::~Linker() {
	reset();
}

void Linker::reset(){
	tokens.clear();
	tableOfSymbols.clear();
	relocationRecords.clear();
	container.clear();
}

void Linker::initialize(ifstream& inputFile) {
	vector<vector<string> > fileTokens;
	int numOfLineForTokens = 0;
	string line;
	while (getline(inputFile, line)) {
		numOfLineForTokens++;
		vector<string> lineTokens = parseInputFile(line);
		if (lineTokens.size() == 0)
			continue;
		fileTokens.push_back(lineTokens);
	}
	tokens.push_back(fileTokens);
}

void Linker::link(){
	bool main = false;
	for (unsigned i = 0; i < tokens.size(); i++){
		vector<vector<string> > fileTokens = tokens[i];
		int fileTokensSize = fileTokens.size();
		Table* fileSymbolTable = new Table();
		Table* fileRelocationTable = new Table();
		vector<vector<string> > fileContent;

		int indexOfLineInFile = 0;
		if (indexOfLineInFile >= fileTokensSize || fileTokens[indexOfLineInFile][0] != "#begin_Table_of_Symbols:")
			throw runtime_error("Syntax error: Bad format.");

		indexOfLineInFile++;
		if (indexOfLineInFile >= fileTokensSize || fileTokens[indexOfLineInFile].size() != 5 ||
				fileTokens[indexOfLineInFile][0] != "label"	|| fileTokens[indexOfLineInFile][1] != "section" ||
				fileTokens[indexOfLineInFile][2] != "offset" || fileTokens[indexOfLineInFile][3] != "locality" ||
				fileTokens[indexOfLineInFile][4] != "serialNum")
			throw runtime_error("Syntax error: Bad format.");

		indexOfLineInFile++;
		while (indexOfLineInFile < fileTokensSize && fileTokens[indexOfLineInFile][0] != "#end_Table_of_Symbols"){
			if (fileTokens[indexOfLineInFile].size() != 5)
				throw runtime_error("Syntax error: Bad format.");

			string label = fileTokens[indexOfLineInFile][0];
			string section = fileTokens[indexOfLineInFile][1];
			string offset = fileTokens[indexOfLineInFile][2];
			string locality = fileTokens[indexOfLineInFile][3];
			string serialNum = fileTokens[indexOfLineInFile][4];

			if (label != "?"){
				if (label[0] == '.'){
					if (!isCorrectName(label.substr(1, label.length()-1)))
						throw runtime_error("Syntax error: Bad format.");
				} else {
					if (!isCorrectName(label))
						throw runtime_error("Syntax error: Bad format.");
				}
			}

			if ((section != "?" && !isCorrectName(section)) || !isNumber(offset) || !isNumber(serialNum))
				throw runtime_error("Syntax error: Bad format.");

			if (locality != "local" && locality != "global" && locality != "extern")
				throw runtime_error("Syntax error: Bad format.");

			if (label == "?" || label[0] == '.' || locality == "global" || locality == "extern"){
				Symbol* symb = new Symbol(label, section, strToInt(offset), locality, strToInt(serialNum));
				fileSymbolTable->add(symb, symb->serialNum);
			}
			if (label == "main" && locality == "global")
				main = true;

			indexOfLineInFile++;
		}
		if (indexOfLineInFile == fileTokensSize)
			throw runtime_error("Syntax error: Bad format.");
		tableOfSymbols.push_back(fileSymbolTable);

		indexOfLineInFile++;
		while (indexOfLineInFile < fileTokensSize && (fileTokens[indexOfLineInFile][0].substr(0, 10)) == "#section_."){
			string section = fileTokens[indexOfLineInFile][0].substr(10, fileTokens[indexOfLineInFile][0].length()-10);
			vector<string> sectionContent;
			sectionContent.push_back(section);
			if (section != "?" && !isCorrectName(section))
				throw runtime_error("Syntax error: Bad format.");

			indexOfLineInFile++;
			if (indexOfLineInFile >= fileTokensSize ||
					(fileTokens[indexOfLineInFile][0] != "#begin_Relocation_Records:" && (fileTokens[indexOfLineInFile][0].substr(0, 14)) != "#section_size_"))
				throw runtime_error("Syntax error: Bad format.");

			if (fileTokens[indexOfLineInFile][0] == "#begin_Relocation_Records:"){

				indexOfLineInFile++;
				if (indexOfLineInFile >= fileTokensSize || fileTokens[indexOfLineInFile].size() != 3 ||
						fileTokens[indexOfLineInFile][0] != "offset" || fileTokens[indexOfLineInFile][1] != "type" ||
						fileTokens[indexOfLineInFile][2] != "serialNum")
					throw runtime_error("Syntax error: Bad format.");

				indexOfLineInFile++;
				while (indexOfLineInFile < fileTokensSize && fileTokens[indexOfLineInFile][0] != "#end__Relocation_Records"){
					if (fileTokens[indexOfLineInFile].size() != 3)
						throw runtime_error("Syntax error: Bad format.");

					string offset = fileTokens[indexOfLineInFile][0];
					string type = fileTokens[indexOfLineInFile][1];
					string serialNum = fileTokens[indexOfLineInFile][2];

					if (!isNumber(offset) || !isNumber(serialNum))
						throw runtime_error("Syntax error: Bad format.");

					if (type != "R_386_16" && type != "R_386_PC16")
						throw runtime_error("Syntax error: Bad format.");

					Relocation* record = new Relocation(section, strToInt(offset), type, strToInt(serialNum));
					fileRelocationTable->add(record, record->serialNum);

					indexOfLineInFile++;
				}
				if (indexOfLineInFile == fileTokensSize)
					throw runtime_error("Syntax error: Bad format.");

				indexOfLineInFile++;
				while (indexOfLineInFile < fileTokensSize && (fileTokens[indexOfLineInFile][0].substr(0, 2)) == "0x"){
					for (unsigned j = 0; j < fileTokens[indexOfLineInFile].size(); j++){
						if ((fileTokens[indexOfLineInFile][j].substr(0, 2)) != "0x")
							throw runtime_error("Syntax error: Bad format.");
						sectionContent.push_back(fileTokens[indexOfLineInFile][j]);
					}
					indexOfLineInFile++;
				}
			} else {
				string ssize = fileTokens[indexOfLineInFile][0].substr(14, fileTokens[indexOfLineInFile][0].length()-14);
				if (!isNumber(ssize))
					throw runtime_error("Syntax error: Bad format.");

				int size = strToInt(ssize);
				for (int j = 0; j < size; j++){
					sectionContent.push_back("0x00");
				}

				indexOfLineInFile++;
			}
			fileContent.push_back(sectionContent);
		}
		if (indexOfLineInFile < fileTokensSize)
			throw runtime_error("Syntax error: Bad format.");
		relocationRecords.push_back(fileRelocationTable);
		container.push_back(fileContent);
	}

	checkIfExistsUnresolvedSymbolsOrMultipleDefinitions();

	if (!main) throw runtime_error("Linker: Label main does not exists.");
}

void Linker::checkIfExistsUnresolvedSymbolsOrMultipleDefinitions(){
	for (unsigned i = 0; i < tableOfSymbols.size(); i++){
		for (tableOfSymbols[i]->onFirst(); tableOfSymbols[i]->isCurrent(); tableOfSymbols[i]->onNext()){
			Symbol* symb1 = (Symbol*) tableOfSymbols[i]->getCurrentData();
			if (symb1->locality == "extern" || symb1->locality == "global"){
				bool ok;
				if (symb1->locality == "extern") ok = false;
				else ok = true;
				for (unsigned j = 0; j < tableOfSymbols.size(); j++){
					if (i != j){
						for (tableOfSymbols[j]->onFirst(); tableOfSymbols[j]->isCurrent(); tableOfSymbols[j]->onNext()){
							Symbol* symb2 = (Symbol*) tableOfSymbols[j]->getCurrentData();
							if (symb2->label == symb1->label && symb2->locality == "global" && symb1->locality == "extern")
								ok = true;
							if (symb2->label == symb1->label && symb2->locality == "global" && symb1->locality == "global"){
								throw runtime_error("Linker: There are multiple definitions.");
							}
						}
					}
				}
				if (!ok) throw runtime_error("Linker: There are unresolved symbols.");
			}
		}
	}
}

vector<string> Linker::parseInputFile(const string &text) {
	string delims = " ,\t"; //not totally correct solution
	vector<string> tokens;
	size_t start = text.find_first_not_of(delims), end = 0;

	while ((end = text.find_first_of(delims, start)) != string::npos) {
		tokens.push_back(text.substr(start, end - start));
		start = text.find_first_not_of(delims, end);
	}
	if (start != string::npos)
		tokens.push_back(text.substr(start));

	return tokens;

}

bool Linker::isLetter(string name) { //ne koristim ovo
	if (name.length() == 0) return false;
	for (unsigned i = 0; i < name.length(); i++) {
		if (!isalpha(name[i])) {
			return false;
		}
	}
	return true;
}

bool Linker::isNumber(string name) {
	if (name.length() == 0) return false;
	for (unsigned i = 0; i < name.length(); i++) {
		if (!isdigit(name[i])) {
			return false;
		}
	}
	return true;
}

bool Linker::isCorrectName(string name) {
	if (name.length() == 0) return false;
	for (unsigned i = 0; i < name.length(); i++) {
		if (!isalnum(name[i]) && name[i] != '_') {
			return false;
		}
	}
	if (isdigit(name[0]) || name[0] == '_') return false;
	return true;
}

int Linker::strToInt(string s){
	int i;
	istringstream (s) >> i;
	return i;
}

